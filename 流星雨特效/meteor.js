var context
var win = { width: 1000, height: 600 }
var starArray = new Array()
var starCount = 800
// 雨滴的容器
var meteorArray = new Array()
// 流星雨数量
var meteorCount = 20

// 初始化画布
var init = function () {
  // 获取canvas
  var _star = document.getElementById('star')
  // 获取当前窗口的宽度、高度
  win.width = window.innerWidth
  win.height = window.innerHeight

  _star.width = win.width
  _star.height = win.height
  console.info(win)

  context = _star.getContext('2d')
}

var star = function () {
  // 随机坐标
  this.x = screen.width * Math.random()
  this.y = 5000 * Math.random()
  this.text = '.'
  this.color = '#fff'

  // 产生随机颜色
  this.getColor = function () {
    var r = Math.random()
    this.color = r < 0.5 ? '#333' : '#fff'
  }

  this.init = function () {
    this.getColor()
  }

  //绘制
  this.draw = function () {
    context.fillStyle = this.color
    context.fillText(this.text, this.x, this.y)
  }
}

// 画星星
var drawstar = function () {
  for (let i = 0; i < starCount; i++) {
    var _star = new star()
    _star.init()
    _star.draw()
    starArray.push(_star)
  }
}

// 画月亮
var drawmoon = function () {
  var moon = new Image()
  moon.src = './images/moon.jpg'
  context.drawImage(moon, -5, -10)
}

var Meteor = function () {
  this.x = -1
  this.y = -1
  this.length = -1 //流星长度
  this.angle = 50 //倾斜角度
  this.width = -1
  this.height = -1
  this.speed = -1 //速度
  this.offsetX = -1 //X 轴偏移量
  this.offsetY = -1
  this.alpha = 1 //透明度
  this.colorX = '' //流星颜色
  this.colorY = ''

  this.init = function () {
    this.getPos()
    this.alpha = 1
    this.getRandomColor()
    var x = Math.random() * 80 + 150
    this.length = Math.ceil(x)
    x = Math.random() + 0.5
    this.speed = Math.ceil(x)
    var cos = Math.cos((this.angle * 3.14) / 180)
    var sin = Math.sin((this.angle * 3.14) / 180)
    this.width = this.length * cos
    this.height = this.length * sin

    this.offsetX = this.speed * cos
    this.offsetY = this.speed * sin
  }

  this.resetPos = function () {
    this.x = this.x - this.offsetX
    this.y = this.y + this.offsetY
  }

  this.getPos = function () {
    this.x = Math.random() * screen.width
    this.y = Math.random() * screen.height
  }

  this.getRandomColor = function () {
    var r = Math.ceil(255 - 240 * Math.random())
    this.colorX = `rgba(${r}, ${r}, ${r}, 1)`
    this.colorY = `rgba(0,0,0,1)`
  }

  this.draw = function () {
    context.save()
    context.beginPath()
    context.lineWidth = 3
    context.globalAlpha = this.alpha

    var line = context.createLinearGradient(
      this.x,
      this.y,
      this.x + this.width,
      this.y - this.height,
    )
    line.addColorStop(0, `#fff`)
    line.addColorStop(0.3, this.colorX)
    line.addColorStop(0.6, this.colorY)
    context.strokeStyle = line

    context.moveTo(this.x, this.y)
    context.lineTo(this.x + this.width, this.y - this.height)
    context.closePath()
    context.stroke()
    context.restore()
  }

  this.move = function () {
    var x = this.x + this.width - this.offsetX
    var y = this.y - this.height
    context.clearRect(x - 3, y - 3, this.offsetX + 5, this.offsetY + 5)
    // 重新计算位置，往左下降落
    this.resetPos()
    this.alpha -= 0.02
    this.draw()
  }
}

var drawmeteor = function () {
  for (let i = 0; i < meteorCount; i++) {
    var _meteor = new Meteor()
    _meteor.init()
    _meteor.draw()
    meteorArray.push(_meteor)
  }
}

var drawanimatemeteor = function () {
  for (let n = 0; n < meteorCount; n++) {
    var _meteor = meteorArray[n]
    _meteor.move()

    if (_meteor.y > win.height) {
      context.clearRect(
        _meteor.x,
        _meteor.y - _meteor.height,
        _meteor.width,
        _meteor.height,
      )
      _meteor[n] = new Meteor()
      _meteor[n].init()
    }
  }
  setTimeout(drawanimatemeteor, 1)
}

window.onload = function () {
  init()
  // 画星星
  drawstar()
  // // 画流星
  drawmeteor()
  // // 画月亮
  // drawmoon()
  // 画流星
  drawanimatemeteor()
}
