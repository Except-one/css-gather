var user = {
  nickname: '落雨成花',
  age: '2000-08-08',
  hobbies: ['玩游戏', '听音乐', '旅游'],
  address: '上海市XXXX路XXX号',
}
observe(user)
mount('#app')

function $(selector) {
  const el = document.querySelectorAll(selector)
  return el.length === 1 ? el[0] : el
}

function showNickname() {
  $('.nickname').innerHTML = user.nickname
}
function showAge() {
  let age = user.age
  if (isString(age)) {
    const timestamp = +new Date() - new Date(age).getTime()
    const yearstamp = 365 * 86400 * 1e3
    age = Math.floor(timestamp / yearstamp)
  }
  $('.age').innerHTML = age
}
function showHobbies() {
  $('.hobbies').innerHTML = user.hobbies.join(', ')
}
function showAddress() {
  $('.address').innerHTML = user.address
}

autorun(showNickname)
autorun(showAge)
autorun(showHobbies)
autorun(showAddress)
