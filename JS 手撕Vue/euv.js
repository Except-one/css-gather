const WINDOW_FUNC = '__func'

function isString(value) {
  return Object.prototype.toString.call(value) === '[object String]'
}

function isObject(value) {
  return Object.prototype.toString.call(value) === '[object Object]'
}

/**
 * 观察者模式 观察对象的变化
 * @param {Object} data
 */
function observe(data) {
  for (const key in data) {
    let internalValue = data[key]
    const funcs = []
    Object.defineProperty(data, key, {
      get() {
        console.log(`属性 ${key} 被读取`, window[WINDOW_FUNC])
        // 依赖收集 window.__func
        if (window[WINDOW_FUNC] && !funcs.includes(window[WINDOW_FUNC])) {
          funcs.push(window[WINDOW_FUNC])
        }
        return internalValue
      },
      set(value) {
        if (value === internalValue) return

        console.log(`属性 ${key} 被修改为 【${value}】`)
        internalValue = value

        // 派发更新
        console.log('%c Line:31 🥐 funcs', 'color:#7f2b82', funcs)
        for (let i = 0; i < funcs.length; i++) {
          funcs[i]()
        }
      },
    })
  }
}

function autorun(fn) {
  window[WINDOW_FUNC] = fn
  fn()
  window[WINDOW_FUNC] = null
}

function mount(el) {
  if (isString(el)) node = document.querySelector(el)
  console.log('%c Line:52 🥝 node', 'color:#42b983', node)
  parseNode(node)
}

function parseNode(node) {
  const count = node.childElementCount
  for (let i = 0; i < count; i++) {
    const child = node.children[i]
    if (child.childElementCount) {
      parseNode(child)
    } else {
      const attr = child.getAttribute(':value')
      setNodeValue(attr, child)
    }
  }
}
function setNodeValue(attr, child) {
  // 格式校验：确保attr是一个非空字符串，并且包含一个有效的`.`分割点
  if (
    typeof attr !== 'string' ||
    attr.trim() === '' ||
    attr.split('.').length !== 2
  ) {
    console.error('Invalid attribute format.')
    return
  }

  const [target, key] = attr.split('.')
  try {
    // 避免直接使用window对象，通过传入参数的方式更安全
    let currentObject = window
    if (!isObject(currentObject[target])) {
      throw new Error('Target object does not exist.')
    }
    currentObject = currentObject[target]

    if (!currentObject[key]) {
      throw new Error(`${key} does not exist in the target object. `)
    }
    child.value = currentObject[key]
  } catch (error) {
    // 异常处理：捕获并记录异常
    console.error('An error occurred while setting the nested value:', error)
  }
}
