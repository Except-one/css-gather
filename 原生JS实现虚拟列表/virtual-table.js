class VirtualTable {
  el = null // 容器元素,可视区域 HTMLElement
  elList = null // 虚拟列表元素 HTMLElement
  options = {} // 配置项
  template = null // 渲染模板函数，用户自定义

  dataSource = [] // 数据源数组

  startIndex = 0
  endIndex = 0
  renderCount = 0 // 渲染的行数
  renderData = [] // 渲染的数据

  scrollStyle = {} // 样式对象

  /**
   * 构造函数
   * @param {HTMLElement} el
   * @param {object} options
   * @param {number} options.itemHeight 行高
   * @param {function} options.template 渲染模板函数
   */
  constructor(el, { data, itemHeight, template }) {
    if (!(el instanceof HTMLElement)) {
      throw new Error(
        'The first parameter to VirtualTable must be a HTMLElement',
      )
    }
    this.el = el

    // 创建虚拟列表
    this.createVirtualNodeList()

    this.options = { itemHeight }
    this.template = template

    this.init(data)
  }
  createVirtualNodeList() {
    const _el = document.createElement('div')
    _el.className = 'virtual-list'
    this.el.appendChild(_el)
    this.elList = _el
  }
  createData(fn) {
    if (Array.isArray(fn)) return fn

    return new Promise((resolve, reject) => {
      if (fn instanceof Function) {
        fn((e) => resolve(e))
      } else {
        reject('The data parameter must be an array')
      }
    })
  }

  async init(fn) {
    this.dataSource = await this.createData(fn)

    // 设置变量
    this.viewHeight = this.el.clientHeight
    this.renderCount = Math.ceil(this.viewHeight / this.options.itemHeight) + 1
    this.endIndex = this.renderCount

    // 初始化数据源
    this.render()
    this.listener()
  }

  addDataSource() {
    for (let i = 0; i < 20; i++) {
      this.dataSource.push({
        id: this.dataSource.length + 1,
        name: `name${this.dataSource.length + 1}`,
      })
    }
  }

  render() {
    this.computed()

    const itemStyle = {
      height: `${this.options.itemHeight}px`,
    }

    for (const key in this.scrollStyle) {
      if (Object.prototype.hasOwnProperty.call(this.scrollStyle, key)) {
        this.elList.style[key] = this.scrollStyle[key]
      }
    }

    const html = this.renderData
      .map((d, i) => this.template(d, i, { ...itemStyle }))
      .join('')
    this.elList.innerHTML = html
  }
  computed() {
    let end = this.startIndex + this.renderCount
    this.endIndex = this.dataSource[end] ? end : this.dataSource.length
    // if (this.endIndex >= this.dataSource.length - 1) {
    // 加载更多数据
    // this.addDataSource()
    // }
    this.renderData = this.dataSource.slice(this.startIndex, this.endIndex)
    // scrollStyle
    const scrollTop = this.options.itemHeight * this.startIndex
    this.scrollStyle = {
      height: `${this.renderData.length * this.options.itemHeight - scrollTop
        }px`,
      transform: `translateY(${scrollTop}px)`,
    }
  }

  listener() {
    this.el.addEventListener(
      'scroll',
      this.throttle(this.handleScroll.bind(this)),
    )
    // window.addEventListener('resize', this.handleResize)
  }

  handleScroll() {
    this.startIndex = Math.floor(this.el.scrollTop / this.options.itemHeight)
    this.render()
  }

  throttle(func, wait = 50) {
    let enabled = !0
    return function () {
      if (enabled) {
        enabled = !1
        requestAnimationFrame(func)
        setTimeout(() => (enabled = !0), wait)
      }
    }
  }
}
