# CSS 聚合

[TOC]

## CSS 文本过长，中间省略，两头正常展示

![CSS 文本过长，中间省略，两头正常展示](./assets/css-text-ellipsis.mp4)

## CSS transition 小技巧

![CSS transition小技巧](./assets/css-transition-tips.mp4)

## CSS 伪类实现圆形比例统计图

![CSS 伪类实现圆形比例统计图](./assets/css-circle-statistical-figure.gif)

## CSS 实现缩放搜索栏

![CSS 实现缩放搜索栏](./assets/css-search-flexible.gif)

## CSS 实现动画登录页面

![CSS 实现动画登录页面](./assets/css-login-animate.gif)

## CSS+JS 实现拟态登录注册

![CSS+JS 实现拟态登录注册](./assets/css-login-minicry.gif)

## 纯 CSS 玻璃态视觉动态登录

![纯CSS 玻璃态视觉动态登录](./assets/css-login-scene.gif)

## CSS 实现二级下拉导航菜单

![CSS 实现二级下拉导航菜单](./assets/css-two-menu.gif)

## CSS 实现伸缩式导航栏

![CSS 实现伸缩式导航栏](./assets/css-bilibili-fixable.gif)

## CSS 垂直卡片滑动动画

![CSS 垂直卡片滑动动画](./assets/css-card-animate.gif)

## CSS 实现输入动画

![CSS 实现输入动画](./assets/css-login-input-animate.gif)

## CSS 实现毛玻璃效果

![CSS 实现毛玻璃效果](./assets/css-ground-glass.gif)
