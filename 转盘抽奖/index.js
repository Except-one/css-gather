function $(selector, ctx = document) {
  const el = ctx.querySelectorAll(selector)
  return el.length === 1 ? el[0] : el
}

/**
 * 抽奖插件
 * @param {Object} options  配置项
 * @param {Element} el      旋转的圆盘DOM（指针不动）
 * @param {Array} data      奖品数据
 * @param {Number} round    转动次数
 * @param {Number} duration 转动时间，默认500ms
 */
class Prize {
  #turning = false
  constructor(options) {
    this.el = options.el
    this.data = options.data
    if (!this.data.length) {
      throw new Error('The configuration item parameter is missing: data Array')
    }
    this.length = this.data.length

    this.round = options.round || 0
    this.duration = options.duration || 5e3
    this.init()
  }

  init() {
    this.#render()
  }

  /**
   * 获取奖品
   *
   * @param {Number} index 奖品索引
   * @returns
   */
  getPrize(index) {
    return this.data[index]
  }

  turn() {
    return new Promise((resolve, reject) => {
      if (this.#turning) {
        reject(new Error('正在获取奖品，请稍后...'))
      } else {
        this.#turning = true
        this.el.classList.add('turning')
        this.round++

        const index = Math.floor(Math.random() * this.length)
        const prerotate = ((index + 1) / this.length + 3 * this.round) * 360
        const deg = prerotate + 360 / (this.length * 2)
        const rotate = `rotate(${deg}deg)`

        this.#css(this.turntable, {
          '-webkit-transform': rotate,
          transform: rotate,
        })

        const timeoutDuration = this.duration + 500
        setTimeout(() => {
          this.#turning = false
          this.el.classList.remove('turning')
          resolve(this.getPrize(index))
        }, timeoutDuration)
      }
    })
  }

  #render() {
    const tempStr = `<div class="turntable"></div><div class="btn"></div>`
    this.el.innerHTML = tempStr
    this.turntable = $('.turntable', this.el)
    this.btn = $('.btn', this.el)

    this.#css(this.turntable, {
      '-webkit-transition': `-webkit-transform ${this.duration}ms`,
      transition: `-webkit-transform ${this.duration}ms`,
      transition: `transform ${this.duration}ms`,
      transition: `transform ${this.duration}ms, -webkit-transform ${this.duration}ms`,
    })

    this.#css(this.btn, {
      '-webkit-transition': '-webkit-transform 0.5s',
      transition: '-webkit-transform 0.5s',
      transition: 'transform 0.5s',
      transition: 'transform 0.5s, -webkit-transform 0.5s',
    })
  }

  #css(el, options) {
    for (let key in options) {
      el.style[key] = options[key]
    }
  }
}
