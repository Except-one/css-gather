window.addEventListener('load', function () {
  const root = document.querySelector('.water-container')
  console.log('%c Line:2 🌶 root', 'color:#fca650', root)
  const inp = document.querySelector('input')

  createStyle()
  setWaterText(+inp.value)
  setWaterHeight(100 - +inp.value)

  inp.oninput = function () {
    setWaterText(+this.value)
    setWaterHeight(100 - +this.value)
  }

  function setWaterText(value) {
    const el = document.querySelector('.water-number')
    el.style.color = value > 25 ? '#abdaad' : '#9e9e9e'
    el.textContent = `${value}%`
  }

  function setWaterHeight(value) {
    if (value === 0) value = -10
    if (value === 100) value = 110
    root.style.setProperty('--water-height', `${value}%`)
  }

  function createStyle() {
    const style = document.createElement('style')
    style.innerHTML = `.water-container {
      --size: 200px;
      --zoom: 4;
      --water-height: 0;
      display: flex;
      align-items: center;
      justify-content: center;
      position: relative;
      left: 50%;
      transform: translateX(-50%);
      overflow: hidden;
      width: var(--size);
      height: var(--size);
      background-color: #ddd;
      border-radius: 50%;
      box-shadow: inset 0 0 10px 0 #666;
    }

    .water-container::after {
      --water: calc(var(--size) * var(--zoom));
      content: "";
      width: var(--water);
      height: var(--water);
      position: absolute;
      top: var(--water-height);
      z-index: 99;
      border-radius: 46% 47% 45% 48%;
      transform: rotate(45deg);
      background-color: rgb(55, 157, 59);
      animation: r360 10s linear infinite;
      box-shadow: inset 0 0 4px 0 #fff;
      transition: .2s all;
    }

    @keyframes r360 {
      from {
        transform: rotate(0deg);
      }

      to {
        transform: rotate(360deg);
      }
    }

    .water-number {
      position: absolute;
      z-index: 999;
      font-size: 16px;
      bottom: 1em;
      color: black;
      transition: 0.2s all;
    }`
    document.head.appendChild(style)
    const img = document.createElement('img')
    document.body.appendChild(img)
    img.offsetWidth
    document.body.removeChild(img)
  }

})
