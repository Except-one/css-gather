function I(selector) {
  return document.querySelector(selector)
}
function I2(selector) {
  return document.querySelectorAll(selector)
}
const doms = {
  swtCtn: I('#switch-cnt'),
  swtC1: I('#switch-c1'),
  swtC2: I('#switch-c2'),
  swtCircle: I2('.switch_circle'),
  swtBtns: I2('button.switch-btn'),
  allBtns: I2('button.submit'),
  aContainer: I('#a-container'),
  bContainer: I('#b-container'),
}

let getButtons = e => e.preventDefault()
let changeForm = e => {
  doms.swtCtn.classList.add('is-gx')
  setTimeout(() => {
    doms.swtCtn.classList.remove('is-gx')
  }, 1500)

  doms.swtCtn.classList.toggle('is-txr')
  doms.swtCircle[0].classList.toggle('is-txr')
  doms.swtCircle[1].classList.toggle('is-txr')

  doms.swtC1.classList.toggle('is-hidden')
  doms.swtC2.classList.toggle('is-hidden')

  doms.aContainer.classList.toggle('is-hidden')
  doms.aContainer.classList.toggle('is-txl')
  doms.bContainer.classList.toggle('is-txl')
}

let loginContainer = e => {
  for (let i = 0; i < doms.allBtns.length; i++) {
    doms.allBtns[i].addEventListener('click', getButtons)
  }
  for (let i = 0; i < doms.swtBtns.length; i++) {
    doms.swtBtns[i].addEventListener('click', changeForm)
  }
}

window.addEventListener('load', loginContainer)
